### 运行
python3 run_inference.py

### 注意
只能在python3.6的环境下运行。

### 导入模型
1. 将从训练云下载的模型文件放到models文件夹下。
2. 改动run_inference.py的17行，确定导入模型的版本  


```python
predictor.restore(os.path.join(prefix, "models/red_player-checkpoint-0"))
```
