from drill.api.bp.network import ActionDecoder
from drill.api.bp.agent.actions import *
from .categorical import CategoricalDecoder
from .single_selective import SingleSelectiveDecoder
from .unordered_multiple_selective import UnorderedMultipleSelectiveDecoder
from .ordered_multiple_selective import OrderedMultipleSelectiveDecoder


def get_decoder_for(action_type):
    """
    this method is needed, because we cannot expose core code(default Decoders) in user's API lib.
    :param action_type: an instance of ActionType
    :return: an instance of ActionDecoder
    """
    assert isinstance(action_type, ActionType)
    if action_type.decoder is not None:
        assert isinstance(action_type.decoder, ActionDecoder)
        return action_type.decoder
    else:
        if isinstance(action_type, CategoricalAction):
            return CategoricalDecoder(config=action_type.decoder_config)
        elif isinstance(action_type, SingleSelectiveAction):
            return SingleSelectiveDecoder(config=action_type.decoder_config)
        elif isinstance(action_type, UnorderedMultipleSelectiveAction):
            return UnorderedMultipleSelectiveDecoder(config=action_type.decoder_config)
        elif isinstance(action_type, OrderedMultipleSelectiveAction):
            return OrderedMultipleSelectiveDecoder(config=action_type.decoder_config)
        else:
            assert False, "no valid ActionDecoder found for action_type {}".format(action_type.name)
